<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
   
require 'lib/mysql1.php';

$db = connect_db();
/*   $sql = "SELECT * FROM fichevisites WHERE 1";
    $exe = $db->query($sql);
    $data = $exe->fetch_all(MYSQLI_ASSOC);
    $db = null;
    print json_encode($data);
*/
  //  var_dump($data);

    if (!empty($_GET['idpays'])) {
       /* $req = "SELECT p.*, u.idpays as pays from utilisateurs u inner join profils p on u.idprofils = p.idprofils where u.idutilisateurs = ".$_GET['idutilisateurs'];

        $result = $db->query($req);
        $res = $result->fetch_array(MYSQLI_ASSOC);
        if ($res['libelle'] == 'Agent animation') {
            $req = "SELECT * FROM pointventes,utilisateurs_pointventes WHERE `pointventes`.`idpointVentes`=`utilisateurs_pointventes`.`idpointVentes` AND  idutilisateurs =".$_GET['idutilisateurs'];
        }
        else if ( $res['libelle'] == 'Directeur Pays' || $res['libelle'] == 'Brand Manager Pays' ||
            $res['libelle'] == 'Responsable Reseaux Pays' ) {
            $req = "SELECT pv.* FROM pointventes pv inner join regions r on pv.idregions = r.idregions WHERE r.idpays = ".$res['pays'];
        } else {
            $req = "SELECT pv.* FROM pointdevente pv WHERE pv.idville=". $_GET['idville'];
        }*/
		
      /*  $req = "SELECT pv.*, v.*".
		" FROM pointdevente pv, ville v WHERE pv.idville= v.idville AND  pv.idville=". $_GET['idville'];*/
		$req = "SELECT pv.*, v.*".
		" FROM pointdevente pv, ville v WHERE  pv.idville = v.idville and v.idville in ".
		"(Select idville from ville where idregion in ".
		"(select idregion from region  where idpays= '".$_GET['idpays']."'))";
        $result = $db->query($req);
        $outp = "";

        while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
            if ($outp != "") {$outp .= ",";}
            /*
            $outp .= '{"pointVente":"'  . json_decode($rs["pointVente"]) . '",';
            $outp .= '"client":"'  . json_decode($rs["client"]) . '",';
            $outp .= '"codePointVente":"'  . json_decode($rs["codePointVente"]) . '"}';
            */
            $outp .= '{"gerant":'  . json_encode($rs["gerant"]) . ',';
            $outp .= '"latitude":'  .$rs["latitude"]. ',';
			$outp .= '"adresse":'  .json_encode($rs["adresse"]). ',';
			$outp .= '"dateAjout":'  .json_encode($rs["dateAjout"]). ',';
			$outp .= '"ville":'  .json_encode($rs["libelle"]). ',';
			
            $outp .= '"longitude":'  .$rs["longitude"]. ',';
            $outp .= '"telephonegerant":'  . json_encode($rs["telephonegerant"]) . '}';

        }
        $outp ='['.$outp.']';

        $db->close();
        print ($outp);
    } else {
        echo "ville non trouvee";
    }

?>