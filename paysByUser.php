<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");


    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
   
require 'lib/mysql1.php';

$db = connect_db();

if (!empty($_GET['idutilisateurs'])) {
    $req = "SELECT p.* FROM utilisateurs u inner join pays p on p.idpays = u.idpays WHERE u.idutilisateurs = ".$_GET['idutilisateurs'];

    $result = $db->query($req);
    $outp = "";


    while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
        if ($outp != "") {$outp .= ",";}
        $outp .= '{"idpays":'  . $rs["idpays"] . ',';
        $outp .= '"code":'  .json_encode($rs["code"]) .',';
        $outp .= '"pays":'  .json_encode($rs["pays"]) .'}';
    }
    $outp ='['.$outp.']';
    $db->close();
    //echo ($outp);

    echo ($outp);
} else  {
    echo "Utilisateur incorrect";
}
    

?>