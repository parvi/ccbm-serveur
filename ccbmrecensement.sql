-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 02 Janvier 2020 à 13:44
-- Version du serveur :  10.1.9-MariaDB
-- Version de PHP :  5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ccbmrecensement`
--

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE `compte` (
  `idcompte` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(25) NOT NULL,
  `idutilisateur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `compte`
--

INSERT INTO `compte` (`idcompte`, `login`, `password`, `idutilisateur`) VALUES
(1, 'admin@gmail.com', 'admin', 1),
(5, 'Bou', '123', 7),
(6, 'Djx', '123', 8),
(7, 'Djx', '123', 9),
(8, 'Ddjddk', '123', 10),
(9, 'Ogi', '', 11),
(10, 'Ogi', '', 12),
(11, 'Sjddw1', '', 13),
(12, 'Qsnsn', '', 14),
(13, 'Cckf', '', 15),
(14, 'Xcclflg', '1234', 16),
(15, 'Rt', '0000', 17),
(16, 'Nnf', '666', 18),
(17, 'Nxnxnf', '000', 19),
(18, 'Nxnxnf', '000', 20),
(19, 'Nckxk', '000', 21),
(20, 'Jxjdjf', '999', 22),
(21, 'Cf', '000', 23),
(22, 'Ggg', '000', 24),
(23, 'Gg', '888', 25),
(24, 'Cc', '000', 26);

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

CREATE TABLE `pays` (
  `idpays` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pays`
--

INSERT INTO `pays` (`idpays`, `libelle`) VALUES
(1, 'Senegal');

-- --------------------------------------------------------

--
-- Structure de la table `pointdevente`
--

CREATE TABLE `pointdevente` (
  `idpointvente` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `gerant` varchar(50) NOT NULL,
  `telephonegerant` varchar(25) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `idville` int(11) NOT NULL,
  `idutilisateur` int(11) NOT NULL,
  `dateAjout` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pointdevente`
--

INSERT INTO `pointdevente` (`idpointvente`, `latitude`, `longitude`, `gerant`, `telephonegerant`, `adresse`, `idville`, `idutilisateur`, `dateAjout`) VALUES
(5, 14.1324524, -17.3654618865, 'tey', '857', 'fdg', 1, 1, '2020-01-01'),
(10, 14.7826497, -17.2720535, 'Skssls', '777777', 'Dkdd', 1, 1, '2020-01-01'),
(11, 14.7833865, -17.2749518, 'Aly', '444444', 'Grand yoff', 1, 1, '2020-01-01'),
(13, 14.7823769, -17.2719476, 'Glgigig', '88777', 'Fif8', 2, 1, '2020-01-01'),
(14, 14.7824272, -17.2719314, 'Moustapha', '33333', 'Thies garr', 2, 1, '2020-01-01'),
(15, 14.782553, -17.2719596, 'F', '8778', 'C', 1, 16, '2020-01-02'),
(16, 14.7539699, -17.492538, 'Moi', '56890', 'Yoff', 1, 7, '2020-01-02'),
(17, 14.7539699, -17.492538, 'Moi', '333', 'Pikine', 1, 7, '2020-01-02'),
(18, 14.7539699, -17.492538, 'De', '22', 'E', 1, 17, '2020-01-02'),
(19, 14.7539699, -17.492538, 'R', '3', 'R', 1, 17, '2020-01-02'),
(23, 14.7539928, -17.4925256, 'Ww', '7', 'Sd', 1, 26, '2020-01-02');

-- --------------------------------------------------------

--
-- Structure de la table `profil`
--

CREATE TABLE `profil` (
  `idprofil` int(11) NOT NULL,
  `libelle` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `profil`
--

INSERT INTO `profil` (`idprofil`, `libelle`, `description`) VALUES
(2, 'Administrateur', 'Administrateur'),
(3, 'Agent recenseur', 'Agent sur le terrain chargé de recensé les point de vente de CCBM');

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

CREATE TABLE `region` (
  `idregion` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL,
  `idpays` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `region`
--

INSERT INTO `region` (`idregion`, `libelle`, `idpays`) VALUES
(1, 'Dakar', 1),
(2, 'Thies', 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `idutilisateur` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `telephone` varchar(25) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `idprofil` int(11) NOT NULL,
  `dateAjout` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idutilisateur`, `nom`, `prenom`, `telephone`, `adresse`, `email`, `idprofil`, `dateAjout`) VALUES
(1, 'admin', 'admin', '776726045', 'adminstrateur', 'admin@gmail.com', 2, '0000-00-00'),
(7, 'Gning', 'Bousso', '778903422', 'Chezmoi', 'Bou', 3, '0000-00-00'),
(8, 'Baba', 'Baba', '555555', 'Cci', 'Djx', 3, '0000-00-00'),
(9, 'Baba', 'Baba', '555555', 'Cci', 'Djx', 3, '2020-01-02'),
(10, 'Dkdd', 'Djdd', '5554', 'Xjdddj', 'Ddjddk', 3, '2020-01-02'),
(11, 'Kckfi', 'Igfif', '7766', 'Ogogog', 'Ogi', 3, '2020-01-02'),
(12, 'Kckfi', 'Igfif', '7766', 'Ogogog', 'Ogi', 3, '2020-01-02'),
(13, 'Xjff', 'Djddd', 'Djddu', 'Djdd', 'Sjddw1', 3, '2020-01-02'),
(14, 'Ddkskk', 'Ssjsj', '777', 'Wnsqn', 'Qsnsn', 3, '2020-01-02'),
(15, 'Xkfjfki', 'Kfkfkf', 'Nxndjfk', 'Cnfk', 'Cckf', 3, '2020-01-02'),
(16, 'Jxkxkc', 'Xjxkf', '5555', 'Kxkxkf', 'Xcclflg', 3, '2020-01-02'),
(17, 'Rr5', 'Err', '666', 'Fr', 'Rt', 3, '2020-01-02'),
(18, 'Bwjnx', 'Ifju', '5555', 'Gkfkfkffk', 'Nnf', 3, '2020-01-02'),
(19, 'Kfjf', 'Jfjf', '666', 'Gkfkg', 'Nxnxnf', 3, '2020-01-02'),
(20, 'Kfjf', 'Jfjf', '666', 'Gkfkg', 'Nxnxnf', 3, '2020-01-02'),
(21, 'Jjffkr', 'Xnx', '777', 'Bnnc', 'Nckxk', 3, '2020-01-02'),
(22, 'Jxjf', 'Nxnf', '6', 'Jfjgk', 'Jxjdjf', 3, '2020-01-02'),
(23, 'Xjgkg', 'Kckvl', '78', 'Lglt', 'Cf', 3, '2020-01-02'),
(24, 'Ccv', 'Cg', '555', '5ggg', 'Ggg', 3, '2020-01-02'),
(25, 'Ggv', 'Ggg', '7', 'Ff', 'Gg', 2, '2020-01-02'),
(26, 'Ggt', 'Fff', '77', 'Fff', 'Cc', 2, '2020-01-02');

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE `ville` (
  `idville` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL,
  `idregion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ville`
--

INSERT INTO `ville` (`idville`, `libelle`, `idregion`) VALUES
(1, 'Dakar plateau', 1),
(2, 'Thies centre', 2);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `compte`
--
ALTER TABLE `compte`
  ADD PRIMARY KEY (`idcompte`),
  ADD KEY `idutilisateur` (`idutilisateur`);

--
-- Index pour la table `pays`
--
ALTER TABLE `pays`
  ADD PRIMARY KEY (`idpays`);

--
-- Index pour la table `pointdevente`
--
ALTER TABLE `pointdevente`
  ADD PRIMARY KEY (`idpointvente`),
  ADD KEY `idville` (`idville`),
  ADD KEY `idutilisateur` (`idutilisateur`);

--
-- Index pour la table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`idprofil`);

--
-- Index pour la table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`idregion`),
  ADD KEY `idpays` (`idpays`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`idutilisateur`);

--
-- Index pour la table `ville`
--
ALTER TABLE `ville`
  ADD PRIMARY KEY (`idville`),
  ADD KEY `idregion` (`idregion`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `compte`
--
ALTER TABLE `compte`
  MODIFY `idcompte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `pays`
--
ALTER TABLE `pays`
  MODIFY `idpays` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `pointdevente`
--
ALTER TABLE `pointdevente`
  MODIFY `idpointvente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `profil`
--
ALTER TABLE `profil`
  MODIFY `idprofil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `region`
--
ALTER TABLE `region`
  MODIFY `idregion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `idutilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT pour la table `ville`
--
ALTER TABLE `ville`
  MODIFY `idville` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `compte`
--
ALTER TABLE `compte`
  ADD CONSTRAINT `compte_ibfk_1` FOREIGN KEY (`idutilisateur`) REFERENCES `utilisateur` (`idutilisateur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `pointdevente`
--
ALTER TABLE `pointdevente`
  ADD CONSTRAINT `pointdevente_ibfk_1` FOREIGN KEY (`idville`) REFERENCES `ville` (`idville`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pointdevente_ibfk_2` FOREIGN KEY (`idutilisateur`) REFERENCES `utilisateur` (`idutilisateur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `region`
--
ALTER TABLE `region`
  ADD CONSTRAINT `region_ibfk_1` FOREIGN KEY (`idpays`) REFERENCES `pays` (`idpays`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `ville`
--
ALTER TABLE `ville`
  ADD CONSTRAINT `ville_ibfk_1` FOREIGN KEY (`idregion`) REFERENCES `region` (`idregion`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
